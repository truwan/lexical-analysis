# Lexical Analysis
> Trubetskov Ivan


Lexical analysis of a subset of Pascal programming language including:

- integer, float and string values
- arithmetic operations (+,-,\*,/)
- goto and labels
- constructions like if-then, if-then-else, while
- built-in functions (sin, abs)


### The Lexing process is based in a deterministic finite-state machine


When the User runs the program, it asks for the name of the file containing the source code.

After the lexing process is done, the program forms the name table and the output sequence in "name\_table.txt" and "output.txt" accordingly.

These files together with the symbol table are input files for the syntax analysis.

Variables have integer type and 0 value by default.

## Example:
The source code:
```
a:= 2.675;
b := -4;
str := "hello";
empty_string := "";

L1: a := b + 0.75;
if a > 0 then
begin
writeln(str);
goto L1;
end
else 
goto L2;

L2: 
writeln("end");
```

The name table:
```
a 1 var int 0
2.675 2 const float 2.675
b 3 var int 0
4 4 const int 4
str 5 var int 0
hello 6 const str hello
empty_string 7 var int 0
 8 const str 
L1 9 const label L1
0.75 10 const float 0.75
0 11 const int 0
L2 12 const label L2
end 13 const str end
```

The output sequence:
```
1 -25 2 -8 3 -25 -4 4 -8 5 -25 6 -8 7 -25 8 -8 9 1 -25 3 -3 10 -8 -20 1 -11 11 -23 -50 -40 -9 5 -10 -8 -43 9 -8 -51 -24 -43 12 -8 12 -40 -9 13 -10 -8 
```

### Dependencies:
* Go 

### TODO:
* Loading the rules from file
* Containing the name table, the output sequence and the symbol table not in *.txt files
