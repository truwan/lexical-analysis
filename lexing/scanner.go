package lexing

import (
	"fmt"
	"unicode"
	"bufio"
	"os"
	"io/ioutil"
	"strings"
	"strconv"
)

var (
SYMBOL = []string{"'","+","-","*","/",",",";","(",")","<",">","\\","[","]","!","#"} // list of all available symbols except ".","_",":","=","{","}"
Sym_table string = "sym_table.txt" // file containing the symbol table
Output_filename string = "output.txt" // file containing the output sequence
Name_table string = "name_table.txt" // file containing the name table
Name_token_code int = 1
Position int = 0
q_num int = 0
)

// check if the character is in the list
func In_list(char string, list []string) bool {
	for _,a := range list {
		if char == a {
			return true
		}
	}
	return false
}

// determine the type of the character
func Define(char string) string{
	if len(char) != 1 {
		return ""
	}
	runechar:=[]rune(char)
	switch {
	case char == ".":
		return "point"
	case char == ":":
		return "colon"
	case char == "{":
		return "com1"
	case char == "}":
		return "com2"
	case char == "\"":
		return "quote"
	case char == "=":
		return "eq"
	case char == "_":
		return "underscore"
	case unicode.IsSpace(runechar[0]):
		return "esc"
	case unicode.IsDigit(runechar[0]):
		return "int"
	case unicode.IsLetter(runechar[0]):
		return "str"
	case In_list(char,SYMBOL):
		return "sym"
	default:
		return "" // unknoown symbol
	}
}

// error handling
func Check_Error(e error){
	if e != nil {
		panic(e)
	}
}

// clear the token
func Null_Lex(token *string) {
	*token = ""
}

// add the character to the token
func Add(token *string, char string){
	*token = *token + char
	if *token == "\"" {
		q_num = q_num + 1 // count quotation marks
	}
}


func Bad_Name_Error() {
	panic("BAD NAME ERROR")
}

func Float_Error() {
	panic("FLOAT ERROR")
}

// write the token code to the file containing the output sequence
func Write_to_output(code string){
	file, err := os.OpenFile(Output_filename,os.O_APPEND|os.O_WRONLY,0333)
	Check_Error(err)
	defer file.Close()

	_,err = file.WriteString(code+" ")
	Check_Error(err)
}

// search for the token in the name table
func Find_In_Names(token *string) string {

	file,err := os.Open(Name_table)
	Check_Error(err)
	defer file.Close()

	scanner:=bufio.NewScanner(file)

	for scanner.Scan() {
		line := string(scanner.Text())
		keyword := strings.Split(line," ")
		if *token == keyword[0] {
			token_code := keyword[1]
			return token_code
		}
	}
	return ""
}

// if the token is not in the name table then put it there
// execute Write_to_output function
func Write_Name(token *string, curr_state string){
	token_tag := ""
	token_value := ""
	token_type := ""
	token_code := ""
	l := *token
	if curr_state == "C2" || curr_state == "C3" {
		*token = l[0:len(l)-2]
	}

	file,err := os.OpenFile(Name_table,os.O_WRONLY|os.O_APPEND,0333)
	Check_Error(err)
	defer file.Close()

	out_file,err := os.Open(Output_filename)
	Check_Error(err)
	defer out_file.Close()


	token_code = Find_In_Names(token)

	if token_code != "" {
		//found 
		Write_to_output(strings.TrimSpace(token_code))
		*token = ""
		return
	}
	// not found, add in the name table
	if (curr_state == "C1" || curr_state == "C3") {
		token_tag = "var"
		token_value = "0"
	} else {
		token_tag = "const"
		token_value = *token
	}

	switch {
	case curr_state == "D1" || curr_state == "C1" || curr_state == "C3":
		token_type = "int"
	case curr_state == "D3":
		token_type = "float"
	case curr_state == "C2":
		token_type = "label"
	default:
		token_type = "str"
	}

	//goto
	scanner := bufio.NewScanner(out_file)
	for scanner.Scan() {
		line := string(scanner.Text())
		keyword := strings.Split(line," ")
		if keyword[len(keyword)-2] == "-43" {
			token_tag = "const"
			token_type = "label"
			token_value = *token
		}
	}

	line := fmt.Sprintf("%s %s %s %s %s",*token,strconv.Itoa(Name_token_code),token_tag,token_type,token_value)
	_,err = file.WriteString(line + "\n")
	Check_Error(err)

	Write_to_output(strings.TrimSpace(strconv.Itoa(Name_token_code)))
	Name_token_code = Name_token_code + 1
	*token = ""
}

// check if the token is a special symbol
// write the token code to the output sequence
func Check_Key(token *string, curr_state string,Position *int) {
	file,err := os.Open(Sym_table)
	Check_Error(err)
	defer file.Close()

	token_code := ""
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := string(scanner.Text())
		keyword := strings.Split(line," ")
		if *token == keyword[0] {
			token_code = keyword[1]
			break
		}
	}

	if token_code != "" {
		if *token == "\"" && q_num%2==0 {
			*Position = *Position + 1
		}
		if *token != "\""{
		Write_to_output(strings.TrimSpace(token_code))
		}
		*token = ""
		return
	}

	Write_Name(token, curr_state)
}


func Ungetch(position *int) {
	*position = *position - 1
}

// execute all procedures from the Procedures list
func Exec_Procedures(token *string, curr_state,char string, position *int, Procedures []string) {

	for _,proc := range Procedures {
		switch {
		case proc == "Add":
			Add(token,char)
		case proc == "Bad_Name_Error":
			Bad_Name_Error()
		case proc == "Float_Error":
			Float_Error()
		case proc == "Write_Name":
			Write_Name(token,curr_state)
		case proc == "Check_Key":
			Check_Key(token,curr_state,position)
		case proc == "Ungetch":
			Ungetch(position)
		case proc == "Null_Lex":
			Null_Lex(token)
		default:
			panic("Unknown procedure")
		}

	}

}

//
func Scanner(filename string){
	// check existance of the file containing the output sequence
	_,err := os.Stat(Output_filename)
	if err == nil {
		//exists
		err = os.Remove(Output_filename)
		Check_Error(err)
	}

	Output_file, err := os.Create(Output_filename)
	Check_Error(err)
	defer Output_file.Close()

	// check existance of the file containing the name table
	_,err = os.Stat(Name_table)
	if err == nil {
		//exists
		err = os.Remove(Name_table)
		Check_Error(err)
	}


	Name_file, err := os.Create(Name_table)
	Check_Error(err)
	defer Name_file.Close()


	Load_Rules() // loading the rules

	//open the file containing the program to analyze
	file,err := os.Open(filename)
	Check_Error(err)
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	Check_Error(err)

	Curr_state := "S" // initial state
	token := ""
	enc_sym := ""

	for Position < len(data){
		enc_sym = Define(string(data[Position]))
		if enc_sym == "" {
			panic("Unknownn symbol")
		}

		for _,rule := range Rules {
			if rule.Q1 == Curr_state {
				if In_list(enc_sym,rule.Encount_sym) {
					Exec_Procedures(&token,Curr_state,string(data[Position]),&Position,rule.Procedures)
					Curr_state = rule.Q2
					break
				}

			}
		}
		Position = Position + 1
		if Position == len(data) {
			if Curr_state == "C1" || Curr_state == "D1" || Curr_state == "D2" || Curr_state == "P1" {
		Check_Key(&token,Curr_state,&Position)
			}
		}
	}
}
