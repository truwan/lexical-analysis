package lexing

type Rule struct {
	Q1 string // current state 
	Q2 string // the next state
	Encount_sym []string // list of encountered symbols
	Procedures []string // list of procedures
}

var Rules []Rule

// loading the rules into Rules array
func Load_Rules() {
	Rules = append(Rules,Rule{"S","S",[]string{"esc"},[]string{"Null_Lex"}})
	Rules = append(Rules,Rule{"S","C1",[]string{"str"},[]string{"Add"}})
	Rules = append(Rules,Rule{"C1","C1",[]string{"str","int","underscore"},[]string{"Add"}})
	Rules = append(Rules,Rule{"C1","S",[]string{"esc","sym","point","eq","quote","com1","com2"},[]string{"Check_Key","Ungetch"}})
	Rules = append(Rules,Rule{"C1","C2",[]string{"colon"},[]string{"Add"}})
	Rules = append(Rules,Rule{"C2","S",[]string{"int","esc","sym","point","str","quote","com1","com2","colon","underscore"},[]string{"Add","Write_Name","Ungetch"}})
	Rules = append(Rules,Rule{"C2","C3",[]string{"eq"},[]string{"Add"}})
	Rules = append(Rules,Rule{"C3","S",[]string{"int","esc","sym","point","str","quote","com1","com2","colon","eq","underscore"},[]string{"Write_Name","Ungetch","Ungetch","Ungetch"}})
	Rules = append(Rules,Rule{"S","D1",[]string{"int"},[]string{"Add"}})
	Rules = append(Rules,Rule{"D1","D1",[]string{"int"},[]string{"Add"}})
	Rules = append(Rules,Rule{"D1","D2",[]string{"point"},[]string{"Add"}})
	Rules = append(Rules,Rule{"D1","E1",[]string{"str"},[]string{"Bad_Name_Error"}})
	Rules = append(Rules,Rule{"D2","D3",[]string{"int"},[]string{"Add"}})
	Rules = append(Rules,Rule{"D3","D3",[]string{"int"},[]string{"Add"}})
	Rules = append(Rules,Rule{"D3","S",[]string{"str","sym","point","eq","colon","esc","quote","com1","com2","underscore"},[]string{"Write_Name","Ungetch"}})
	Rules = append(Rules,Rule{"D2","E2",[]string{"str","sym","point","eq","colon","esc","quote","com1","com2","underscore"},[]string{"Float_Error"}})
	Rules = append(Rules,Rule{"D1","S",[]string{"sym","eq","colon","esc","quote","com1","com2","underscore"},[]string{"Write_Name","Ungetch"}})
	Rules = append(Rules,Rule{"S","S",[]string{"sym","point","eq","underscore"},[]string{"Add","Check_Key"}})
	Rules = append(Rules,Rule{"S","M1",[]string{"com1"},[]string{}})
	Rules = append(Rules,Rule{"M1","M1",[]string{"int","str","sym","point","eq","colon","esc","quote","com1","underscore"},[]string{}})
	Rules = append(Rules,Rule{"M1","S",[]string{"com2"},[]string{}})
	Rules = append(Rules,Rule{"S","Q1",[]string{"quote"},[]string{"Add","Check_Key"}})
	Rules = append(Rules,Rule{"Q1","Q1",[]string{"int","str","point","sym","eq","colon","esc","com1","com2","underscore"},[]string{"Add"}})
	Rules = append(Rules,Rule{"Q1","S",[]string{"quote"},[]string{"Write_Name","Ungetch","Add","Check_Key"}})
	Rules = append(Rules,Rule{"S","P1",[]string{"colon"},[]string{"Add"}})
	Rules = append(Rules,Rule{"P1","S",[]string{"eq"},[]string{"Add","Check_Key"}})
	Rules = append(Rules,Rule{"P1","S",[]string{"int","point","str","sym","colon","esc","quote","com1","com2","underscore"},[]string{"Check_Key","Ungetch"}})
}
