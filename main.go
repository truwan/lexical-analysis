package main

import(
	"fmt"
	"os"
	"github.com/mattn/go-gtk/gtk"
	"github.com/truwan/Lexical_Analysis/lexing"
)

func main() {
    gtk.Init(&os.Args)
    window := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
    window.SetTitle("Lexical Analysis")
    window.SetSizeRequest(700,500)
    window.SetPosition(1)
    window.Connect("destroy", func() {
        gtk.MainQuit()})

// creating dialog window to get the name of the file containing the program to analyze
    dialog := gtk.NewDialog()
    dialog.SetSizeRequest(400,200)
    dialog.SetTitle("ENTER THE NAME OF THE FILE:")
    dVbox := dialog.GetVBox()

    input := gtk.NewEntry()
    input.SetEditable(true)
    dVbox.Add(input)
    vbox := gtk.NewVBox(false, 1)

    input.Connect("activate", func() {
    filename := input.GetText()
    fmt.Print(filename)
    lexing.Scanner(filename) // run lexical analysis
    dialog.Destroy()
    gtk.MainQuit()
    })

    button := gtk.NewButtonWithLabel("OK")
    button.Connect("clicked", func() {
    input.Emit("activate")
    })
    dVbox.Add(button)
    dialog.SetModal(true)
    dialog.ShowAll()


    window.Add(vbox)
    window.ShowAll()
    gtk.Main()


}
